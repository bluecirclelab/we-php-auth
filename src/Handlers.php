<?php

namespace blueCircleLab\WorshipExtremeAuth;

use blueCircleLab\WorshipExtremeAuth\Helpers\Cookies;
use blueCircleLab\WorshipExtremeAuth\Helpers\Env;
use blueCircleLab\WorshipExtremeAuth\Helpers\ErrorHandler;
use blueCircleLab\WorshipExtremeAuth\Provider\WorshipExtremeProvider;
use Slim\Http\Response as Response;
use Slim\Http\ServerRequest as Request;
use Slim\Psr7\Cookies as HTTPCookies;

class Handlers {

  public static function logOut(Request $request, Response $response): Response {

    $cookies = new HTTPCookies($request->getCookieParams());

    // revoke refresh token
    if ($cookies->get(Cookies::REFRESH_COOKIE)) {
      $options = [
          'http' => [
              'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
              'method'  => 'POST',
              'content' => http_build_query([
                  'client_id' => getenv('WE_AUTH_CLIENT'),
                  'token' => $cookies->get(Cookies::REFRESH_COOKIE)
              ])
          ]
      ];
      $context = stream_context_create($options);
      file_get_contents(Env::getAuthDomain().'/revoke', false, $context);
    }

    // remove cookies
    $responseCookies = Cookies::setTokenCookies();
    return $response->withHeader('Set-Cookie', $responseCookies->toHeaders());

  }

  public static function callback(Request $request, Response $response): Response {

    $cookies = new HTTPCookies($request->getCookieParams());
    if($cookies->get(Cookies::STATE_COOKIE) !== $request->getQueryParam('state')) {
      return (new ErrorHandler($response))->send('State mismatch', 401);
    }

    $provider = new WorshipExtremeProvider($request);
    try {
      // Get an access token using the authorization code grant.
      $accessToken = $provider->getAccessToken('authorization_code', [
          'code' => $request->getQueryParam('code'),
          'remote_ip' => Env::getClientIp(),
          'device_os' => Env::getOS(),
          'device_name' => Env::getBrowser(),
      ]);

      $responseCookies = Cookies::setTokenCookies($accessToken->getToken(), $accessToken->getRefreshToken());

      // remove state and redirect cookies
      $responseCookies->set(Cookies::STATE_COOKIE, [
          'httponly' => true,
          'secure' => !Env::isDev(),
          'path' => '/',
          'expires' => strtotime('-1 day'),
      ]);
      $responseCookies->set(Cookies::REDIRECT_COOKIE, [
          'httponly' => true,
          'secure' => !Env::isDev(),
          'path' => '/',
          'expires' => strtotime('-1 day'),
      ]);

      return $response
          ->withHeader('Set-Cookie', $responseCookies->toHeaders())
          ->withRedirect(getenv('WE_AUTH_SUCCESS_REDIRECT') ?: ($cookies->get(Cookies::REDIRECT_COOKIE) ?: '/'));

    } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
      return (new ErrorHandler($response))->send($e->getMessage(), 401);
    }
  }

}
