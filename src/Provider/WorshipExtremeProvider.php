<?php
namespace blueCircleLab\WorshipExtremeAuth\Provider;

use blueCircleLab\WorshipExtremeAuth\Helpers\Env;
use League\OAuth2\Client\Provider\GenericProvider;
use Slim\Http\ServerRequest as Request;

class WorshipExtremeProvider extends GenericProvider {

  public function __construct(Request $request = null) {
    $isClientGrant = is_null($request);
    parent::__construct(array_filter([
        'clientId'                => getenv('WE_AUTH_CLIENT'),
        'clientSecret'            => getenv('WE_AUTH_SECRET'),
        'redirectUri'             => (!$isClientGrant && getenv('WE_AUTH_CALLBACK')) ?
          (string) $request->getUri()
          ->withPath(getenv('WE_AUTH_CALLBACK'))
          ->withPort(Env::isDev() ? $request->getUri()->getPort() : null)
          ->withQuery('') : null,
        'urlAuthorize'            => Env::getAuthDomain().'/authorize',
        'urlAccessToken'          => Env::getAuthDomain().'/access_token',
        'urlResourceOwnerDetails' => '',
        'scopes'                  => getenv('WE_AUTH_SCOPES')
    ], function($value) {
      return $value !== false;
    }), getenv('WE_AUTH_DISABLE_CERT_VERIFY') ? ['httpClient' => new \GuzzleHttp\Client(['verify' => false])] : []);
  }

}
