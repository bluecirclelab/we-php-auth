<?php
namespace blueCircleLab\WorshipExtremeAuth\Helpers;

class ErrorHandler {

  private $response;

  public function __construct($response) {
    $this->response = $response;
  }

  public function send($message, $code = 400) {
    return $this->response->withStatus($code)->withJson([
      'error' => [
        'status' => $code,
        'message' => $message
      ]
    ]);
  }
}
