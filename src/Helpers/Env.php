<?php

namespace blueCircleLab\WorshipExtremeAuth\Helpers;

use donatj\UserAgent\UserAgent;
use donatj\UserAgent\UserAgentParser;

class Env {

  const DEFAULT_AUTH_DOMAIN = 'https://auth.worshiptools.com';

  private static $parser;

  private static function getParser(): UserAgent {
    if (!isset(self::$parser)) {
      $parser = new UserAgentParser();
      self::$parser = $parser();
    }
    return self::$parser;
  }

  public static function isDev(): bool {
    return isset($_SERVER['SERVER_SOFTWARE']) && strpos($_SERVER['SERVER_SOFTWARE'], 'Development Server') !== false;
  }

  public static function getAuthDomain(): string {
    return getenv('WE_AUTH_DOMAIN') ?: self::DEFAULT_AUTH_DOMAIN;
  }

  public static function getPublicKey() {
    $cachePath = sys_get_temp_dir() . '/wt-auth-public-'.md5(self::getAuthDomain()).'.key';
    if (defined('DEV_ENV') && DEV_ENV) {
      error_log('Auth Public Key: '.$cachePath);
    }
    if (file_exists($cachePath)) {
      return file_get_contents($cachePath);
    }

    $request = (new \GuzzleHttp\Client([
      'headers' => [ 'User-Agent' => getenv('WE_AUTH_CLIENT') ]
    ]))->request('GET', self::getAuthDomain().'/key');
    $key = base64_decode($request->getBody());
    file_put_contents($cachePath, $key);

    return $key;
  }

  public static function getClientIp(): string {
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) {
      return strtok($_SERVER['HTTP_X_FORWARDED_FOR'], ',');
    }
    return $_SERVER['REMOTE_ADDR'] ?? '';
  }

  public static function getBrowser(): string {
    $ua = self::getParser();
    return implode(' ', array_filter([
        $ua->browser(),
        $ua->browserVersion(),
    ]));
  }

  public static function getOS(): string {
    $ua = self::getParser();
    return $ua->platform() ?: '';
  }
}
