<?php

namespace blueCircleLab\WorshipExtremeAuth\Helpers;

use Slim\Psr7\Cookies as HTTPCookies;

class Cookies {

  const TOKEN_COOKIE = 'weAuthToken';
  const REFRESH_COOKIE = 'weAuthRefresh';
  const STATE_COOKIE = 'weAuthState';
  const REDIRECT_COOKIE = 'weAuthRedirect';

  public static function setTokenCookies(string $accessToken = '', string $refreshToken = '', HTTPCookies $cookies = null) {
    $responseCookies = $cookies ?: new HTTPCookies();
    $responseCookies->set(Cookies::TOKEN_COOKIE, [
        'value' => $accessToken,
        'httponly' => false,
        'secure' => !Env::isDev(),
        'path' => '/',
        'samesite' => 'Lax',
        'expires' => $accessToken ? null : strtotime('-1 day')
    ]);
    $responseCookies->set(Cookies::REFRESH_COOKIE, [
        'value' => $refreshToken,
        'httponly' => true,
        'secure' => !Env::isDev(),
        'path' => '/',
        'samesite' => 'Lax',
        'expires' => $refreshToken ? strtotime('+'.(getenv('WE_AUTH_REFRESH_COOKIE_LENGTH') ?: '24 hours')) : strtotime('-1 day')
    ]);

    return $responseCookies;
  }
}
