<?php
namespace blueCircleLab\WorshipExtremeAuth\Middleware;

use blueCircleLab\WorshipExtremeAuth\Helpers\Env;
use blueCircleLab\WorshipExtremeAuth\Helpers\Cookies;
use blueCircleLab\WorshipExtremeAuth\Provider\WorshipExtremeProvider;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Http\ServerRequest as Request;
use Slim\Psr7\Cookies as HTTPCookies;
use Slim\Psr7\Response;

class Auth {

  const REDIRECT_TO_LOGIN = 0;
  const FAIL_AND_ABORT = 1;
  const CONTINUE_EXECUTION = 2;

  private $failBehavior;
  private $successfulRedirectUrl;

  /**
   * @param integer $failBehavior
   * @param string $successfulRedirectUrl
   */
  public function __construct($failBehavior = self::REDIRECT_TO_LOGIN, $successfulRedirectUrl = null) {
    $this->failBehavior = $failBehavior;
    $this->successfulRedirectUrl = $successfulRedirectUrl;
  }

  public function __invoke(Request $request, RequestHandler $handler) {

    // if prefetch request, ignore
    if ($request->hasHeader('purpose') && $request->getHeader('purpose')[0] === 'prefetch') {
      return (new Response())->withStatus(204);
    }

    $provider = new WorshipExtremeProvider($request);
    $cookies = new HTTPCookies($request->getCookieParams());
    $session = null;
    $accessToken = $cookies->get(Cookies::TOKEN_COOKIE);

    $key = Env::getPublicKey();
    $key = new Key($key, 'RS256');

    try {
      JWT::$leeway = 15;
      $session = JWT::decode($accessToken, $key);
    } catch (\Firebase\JWT\ExpiredException | \Throwable $e) {
      if ($cookies->get(Cookies::REFRESH_COOKIE)) {
        try {
          $newAccessToken = $provider->getAccessToken('refresh_token', [
            'refresh_token' => $cookies->get(Cookies::REFRESH_COOKIE),
            'remote_ip' => Env::getClientIp(),
            'device_os' => Env::getOS(),
            'device_name' => Env::getBrowser(),
          ]);
          $accessToken = $newAccessToken->getToken();
          $session = JWT::decode($accessToken, $key);
          $responseCookies = Cookies::setTokenCookies($accessToken, $newAccessToken->getRefreshToken());
        } catch (\Throwable $e) {
          // refresh failed
        }
      }
    }

    if (
      $session && (
        $session->aud === getenv('WE_AUTH_CLIENT') ||
        (getenv('WE_AUTH_ADDITIONAL_VALID_CLIENTS') && in_array($session->aud, explode(',', getenv('WE_AUTH_ADDITIONAL_VALID_CLIENTS'))))
      )) {
      $response = $handler->handle($request
        ->withAttribute('accessToken', $accessToken)
        ->withAttribute('session', $session));
      if (isset($responseCookies)) {
        $response = $response->withHeader('Set-Cookie', $responseCookies->toHeaders());
      }
      return $response;
    }

    if ($this->failBehavior === self::FAIL_AND_ABORT) {
      return (new Response())->withStatus(401);
    }

    if ($this->failBehavior === self::REDIRECT_TO_LOGIN) {

      $authorizationUrl = $provider->getAuthorizationUrl();

      // remove any existing access or refresh cookies
      $responseCookies = Cookies::setTokenCookies();

      $responseCookies->set(Cookies::STATE_COOKIE, [
        'value' => $provider->getState(),
        'httponly' => true,
        'secure' => !Env::isDev(),
        'path' => '/',
        'samesite' => 'Lax',
      ]);

      // if redirect path is not set, redirect user back to route they wanted to access
      if (!getenv('WE_AUTH_SUCCESS_REDIRECT') && $request->getMethod() === 'GET') {
        $responseCookies->set(Cookies::REDIRECT_COOKIE, [
          'value' => $this->successfulRedirectUrl ?: (string) $request->getUri()->withPort(Env::isDev() ? $request->getUri()->getPort() : null),
          'httponly' => true,
          'secure' => !Env::isDev(),
          'path' => '/',
          'samesite' => 'Lax',
        ]);
      }

      return (new Response())
        ->withHeader('Set-Cookie', $responseCookies->toHeaders())
        ->withStatus(302)
        ->withHeader('Location', $authorizationUrl);
    }

    return $handler->handle($request);

  }

}
