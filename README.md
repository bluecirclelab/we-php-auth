# WorshipTools Auth Middleware

PHP Middleware wrapper for adding WorshipTools authentication to PHP apps.

## Installation

Add the following to composer.json:

```json
{
  "repositories": [
    {
      "url": "https://bitbucket.org/bluecirclelab/we-php-auth.git",
      "type": "git"
    }
  ],
  "require": {
    "bluecirclelab/we-php-auth": "dev-master"
  }
}
```

## Usage

### Set env vars

```yaml
  # Required
  WE_AUTH_CLIENT: 'weClient'
  WE_AUTH_SECRET: '...'
  WE_AUTH_SCOPES: 'user.info.read'
  
  # Optional
  WE_AUTH_DOMAIN: 'https://auth.worshiptools.com'
  WE_AUTH_CALLBACK: '/auth/callback'
  WE_AUTH_REFRESH_COOKIE_LENGTH: '7 days'
  WE_AUTH_SUCCESS_REDIRECT: '/app'
```

### Protecting a route
```php
  $app->get('/secure/route', function (Request $request, Response $response) {
    // ...
  })->add( new blueCircleLab\WorshipExtremeAuth\Middleware\Auth()  );
```

### Callback and Logout
```php
  $app->get('/auth/logout', function (Request $request, Response $response) {
    $response = \blueCircleLab\WorshipExtremeAuth\Handlers::logout($request, $response);
    return $response
        ->withRedirect('https://www.worshiptools.com', 302);
  });

  $app->get('/auth/callback', function (Request $request, Response $response) {
    return \blueCircleLab\WorshipExtremeAuth\Handlers::callback($request, $response);
  });
```
